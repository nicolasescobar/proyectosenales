T=readtable('Casos_positivos_de_COVID-19_en_Colombia.csv','VariableNamingRule','preserve');
edad={'menores de 10' 'entre 10 y 20' 'entre 20 y 30' 'entre 30 y 40' 'mayores de 40'};
loc={'Bogotá D.C.' 'Cali' 'Barranquilla' 'Bucaramanga' 'Medellín'};
for i=1:5
    w4=contains(T{:,4},loc(i));
    x(i)=length(T{:,4}(w4));
    
end
x2=categorical(table2array(T(:,4)),loc);
dias=unique(T{:,2});
for i=2:length(dias)
    w4=contains(T{:,2},dias(i));
    y(i)=length(T{:,2}(w4));
end
w4=(T{:,7}<=10);
z(1)=length(T{:,7}(w4));
w4=(T{:,7}>10 & T{:,7}<=20);
z(2)=length(T{:,7}(w4));
w4=(T{:,7}>20 & T{:,7}<=30);
z(3)=length(T{:,7}(w4));
w4=(T{:,7}>30 & T{:,7}<=40);
z(4)=length(T{:,7}(w4));
w4=(T{:,7}>40);
z(5)=length(T{:,7}(w4));
subplot(2,2,1)
pie(z)
legend(edad)
title('Clasificación de casos por edad')
y1=1:length(dias);
subplot(2,2,2)
plot(y1,y)
title('Linea de tiempo de casos en el país')
xlabel('dias desde el primer caso')
ylabel('número de casos')
subplot(2,2,3)
histogram(x2)
title('Clasificación de casos por ciudad')
ylabel('número de casos')